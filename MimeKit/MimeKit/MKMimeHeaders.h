//
//  MKMimeHeaders.h
//  MKMimeKit
//
//  Created by Scott Morrison on 11-01-30.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//


#import <Cocoa/Cocoa.h>

#define kMessageHeaderDateKey @"Date"
#define kMessageHeaderFromKey @"From"
#define kMessageHeaderSenderKey @"Sender"
#define kMessageHeaderToKey @"To"
#define kMessageHeaderCcKey @"Cc"
#define kMessageHeaderBccKey @"Bcc"
#define kMessageHeaderMessageIdKey @"Message-Id"
#define kMessageHeaderSubjectKey @"Subject"

@interface MKMimeHeaders : NSObject
@property (readonly) NSDictionary * headerDictionary; /* Retained */
@property (assign) NSStringEncoding preferredEncoding;

+ (NSString*)mimeEncodedHeaderForKey:(NSString*)headerKey
                               value:(NSString*)string
                   preferredEncoding:(NSStringEncoding)encoding
                        encodingUsed:(NSStringEncoding*)usedEncoding;

- (id)initWithHeaderDictionary:(NSDictionary *)aHeaderDictionary;

- (NSString*)firstHeaderForKey:(NSString *)key;

- (NSString*)unfoldedHeaderString;

// * foldedHeaderString returns a header string properly wrapped at 76 characters
//   characters and properly mime encoded with QEncoding.

- (NSString*)foldedHeaderString;

- (void)setHeader:(NSString*)headerValue forKey:(NSString*)headerKey;

@end
