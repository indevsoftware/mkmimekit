//
//  NSString+MailKit.m
//  MKMimeKit
//
//  Created by smorr on 2016-02-13.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "NSString+MailKit.h"

NSString *MKStringWithBits(int64_t mask) {
    NSMutableString *mutableStringWithBits = [NSMutableString new];
    for (int8_t bitIndex = 0; bitIndex < sizeof(mask) * 8; bitIndex++) {
        [mutableStringWithBits insertString:mask & 1 ? @"1" : @"0" atIndex:0];
        mask >>= 1;
    }
    return [mutableStringWithBits copy];
}

NSString * MKStringWithUInteger(NSUInteger value){
    char stringBuffer[32] = ""; // NSUIntegerMax = 18446744073709551615  //20 placeValues + null
    snprintf(stringBuffer, 32, "%lu",(unsigned long)value);
    return [NSString stringWithCString:stringBuffer encoding:NSASCIIStringEncoding];
}

@implementation NSString (MailKit)
-(NSString*)quotedString{
    return [NSString stringWithFormat:@"\"%@\"",self];
}

-(NSString*)capitalizedHeaderKey{
    NSArray <NSString*>* parts = [self componentsSeparatedByString:@"-"];
    NSMutableArray * newParts = [NSMutableArray array];
    for (NSString* part in parts){
        [newParts addObject: [part capitalizedString]];
    }
    return [newParts componentsJoinedByString:@"-"];
    
}
@end
