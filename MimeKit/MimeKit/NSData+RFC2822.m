//
//  NSData+RFC2822.m
//  MKMimeKit
//
//  Created by smorr on 2015-08-04.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "NSData+RFC2822.h"

@implementation NSData (RFC2822)
-(NSRange) rangeOfRFC2822HeaderData{
    static NSData * crlfData_2 = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        crlfData_2 = [NSData dataWithBytes:"\r\n\r\n" length:4];
    });
    NSRange crlfRange = [self rangeOfData:crlfData_2 options:0 range:NSMakeRange(0, [self length])];
    if (crlfRange.location!=NSNotFound){
        NSRange headerRange = NSMakeRange(0, crlfRange.location);
        return headerRange;
    }
    else{
        return NSMakeRange(0,0);
    }
}

-(NSData*)headerSubdata{
    NSRange rangeOf822Headers = [self rangeOfRFC2822HeaderData];
    if (rangeOf822Headers.length){
        return [self subdataWithRange:rangeOf822Headers];
    }
    else{
        return nil;
    }
}

-(NSData*)bodySubdata{
    NSRange rangeOf822Headers = [self rangeOfRFC2822HeaderData];
    if (rangeOf822Headers.length) {
        NSUInteger start = rangeOf822Headers.length+4;
        return [self subdataWithRange:NSMakeRange(start,[self length]-start)];
    }
    return nil;
}

-(NSData*) dataByTrimmingBytesInCharacterSet:(NSCharacterSet*)charSet{
    NSUInteger startLocation = 0;
    NSUInteger endLocation = self.length;
    unichar byte = '\0';
    while (startLocation < self.length){
        [self getBytes: &byte range:NSMakeRange(startLocation,1)];
        if ([charSet characterIsMember:byte]){
            startLocation++;
        }
        else{
            break;
        }
    }
    
    while (endLocation>0){
        [self getBytes: &byte range:NSMakeRange(endLocation-1,1)];
        if ([charSet characterIsMember:byte]){
            endLocation--;
        }
        else{
            break;
        }
    }
    if (endLocation>startLocation){
        return [self subdataWithRange:NSMakeRange(startLocation, endLocation-startLocation+1)];
    }
    return nil;
}


-(NSArray <NSData*> *) componentsSeparatedByData:(NSData*)delimiterData{
//    NSData * boundarydata = [[@"\n--" stringByAppendingString:[mimePart boundary]] dataUsingEncoding:NSASCIIStringEncoding];
//    NSData *contentData= [parser contentData];
    
    
    NSMutableArray <NSData*> * components = [NSMutableArray array];
    NSRange searchRange = NSMakeRange(0, [self length]);
    NSRange delimiterRange;// = NSMakeRange(NSNotFound, 0);
    
    do {
        delimiterRange = [self rangeOfData:delimiterData options:0 range:searchRange];
        if (delimiterRange.location==NSNotFound){
            break;
        }
        NSData * componentData = [self subdataWithRange:NSMakeRange(searchRange.location, delimiterRange.location - searchRange.location)];
        if (componentData){
            [components addObject:componentData];
        }
        searchRange.location = delimiterRange.location+delimiterRange.length;
        searchRange.length = self.length - searchRange.location;
        
    }while (delimiterRange.location!=NSNotFound);
    
    return components;
    
}

-(NSString*) asciiString{
    return [[NSString alloc] initWithData:self encoding:NSASCIIStringEncoding];
}
-(NSString*) utf8String{
    return [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
}
-(NSString*) latin1String{
    return [[NSString alloc] initWithData:self encoding:NSISOLatin1StringEncoding];
}



@end
