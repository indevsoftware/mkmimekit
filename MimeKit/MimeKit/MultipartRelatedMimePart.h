//
//  MultipartRelatedMimePart.h
//  Martlet
//
//  Created by smorr on 10-03-2016.
//
//

#import "MultipartMimePart.h"
#import "MKCommon.h"

@interface MultipartRelatedMimePart : MultipartMimePart
-(instancetype)initWithHtmlPart:(MimePart*) htmlPart relatedParts:(NSArray _of(MimePart)*)relatedParts;
    
@end
