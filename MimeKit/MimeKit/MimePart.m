//
//  MimePart.m
//  MKMimeKit
//
//  Created by Scott Morrison on 11-01-29.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "MimePart.h"

#import <WebKit/WebKit.h>

#import "NSData+Base64.h"
#import "NSData+RFC2822.h"
#import "NSString+MD5.h"
#import "MKMimeHeadersParser.h"


//Mime header keys 
NSString * const kMimeHeaderContentTransferEncoding = @"Content-Transfer-Encoding";
NSString * const kMimeHeaderContentType = @"Content-Type";
NSString * const kMimeHeaderContentID = @"Content-Id";
NSString * const kMimeHeaderContentDisposition = @"Content-Disposition";

// Transfer encodings
NSString * const kMimeTransferEncodingBase64 = @"base64";
NSString * const kMimeTransferEncoding8Bit = @"8bit";
NSString * const kMimeTransferEncoding7Bit = @"7bit";
NSString * const kMimeTransferEncodingQuotedPrintable = @"quoted-printable";

// content encoding
NSString * const kMimeContentEncodingISO8859 = @"iso-8859-1";
NSString * const kMimeContentEncodingUTF8 = @"utf-8";
NSString * const kMimeContentEncodingUSASCII = @"us-ascii";

@interface MimePart ()
@property (copy) NSString* internalContentTransferEncoding;
@property (copy) NSString* internalCharset;
@end


static NSMutableDictionary <NSString*,NSString*> * _sMimeClassNamesByType;
static NSObject * _sLock;

@implementation MimePart (ClassMethods)

+ (void)registerForMimeType:(NSString*)mimeType{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sLock = [[NSObject alloc] init];
        _sMimeClassNamesByType = [[NSMutableDictionary alloc] init];
    });
    
    @synchronized(_sLock) {
        _sMimeClassNamesByType[[mimeType lowercaseString]] = NSStringFromClass(self);
    }
}

+ (Class)classForMimeType:(NSString*)mimeType{
    Class returnClass = nil;
    NSArray * typesComponents = [mimeType componentsSeparatedByString:@"/"];
    if ([[mimeType lowercaseString] hasPrefix:@"application"]){
        
    }
    NSString * baseType = nil;
    if ([typesComponents count]){
        baseType = [NSString stringWithFormat:@"%@/*",[typesComponents[0] lowercaseString]];
    }
    @synchronized(_sLock) {
        NSString* className = _sMimeClassNamesByType[mimeType];
        if (!className){
            className = _sMimeClassNamesByType[baseType];
        }
        if (className){
            returnClass = NSClassFromString(className);
        }
    }
    
    return returnClass?:[MimePart class];
}

+ (MimePart *)mimePartForData:(NSData *)mimeData {
    MKMimeHeaders * headers = [MKMimeHeadersParser headersFromData:[mimeData headerSubdata]];
    NSString * contentTypeHeader = [headers firstHeaderForKey:@"Content-Type"];
    NSString * typeSubType = nil;
    
    if (contentTypeHeader){
        NSArray * contentTypeHeaderParts = [contentTypeHeader componentsSeparatedByString:@";"];
        if ([contentTypeHeaderParts count]){
            typeSubType = [[contentTypeHeaderParts objectAtIndex:0] lowercaseString];
        }
    }
    if ([typeSubType isEqualToString:@"text"]){
        
    }
    Class MimeClass = [self classForMimeType:typeSubType];
    MimePart  * mimePart =[[MimeClass alloc] initWithMimeData:mimeData];
    return mimePart;
}

@end


@implementation MimePart

- (id)initWithMimeData:(NSData *)mimeData {
	self = [self init];
    
	if (self) {
        
        MKMimeHeaders * headers = [MKMimeHeadersParser headersFromData:[mimeData headerSubdata]];
        NSString * contentDispositionString = [headers firstHeaderForKey:kMimeHeaderContentDisposition] ;
        NSString * contentTypeHeader = [headers firstHeaderForKey:kMimeHeaderContentType]; // nb do not transform to lowercase string
        NSString * contentTransferEncoding = [[headers firstHeaderForKey:kMimeHeaderContentTransferEncoding] lowercaseString];
        NSString * contentId =  [headers firstHeaderForKey:kMimeHeaderContentID]; // nb do not transform to lowercase string
    
        self.headers = headers;
        self.contentTransferEncoding= contentTransferEncoding;
        self.contentId = contentId;
        
        if (contentDispositionString) {
            NSArray * contentDispositionComponents = [contentDispositionString componentsSeparatedByString:@";"];
            NSMutableDictionary * contentDispositionDictionary = [[NSMutableDictionary alloc] init];
            
            for (NSString * contentDispositionComponent in contentDispositionComponents) {
                NSArray * parts = [contentDispositionComponent componentsSeparatedByString:@"="];
                if ([parts count] == 1) {
                    contentDispositionDictionary[@"type"] = parts[0];
                }
                else {
                    NSString * key = [[parts[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
                    contentDispositionDictionary[key] = parts[1] ;
                }
            }
            self.contentDisposition = contentDispositionDictionary;
        }
        
        if (contentTypeHeader) {
            NSArray * contentTypeHeaderParts = [contentTypeHeader componentsSeparatedByString:@";"];
            NSInteger componentCount = [contentTypeHeaderParts count];
            NSString * typeSubType = [contentTypeHeaderParts[0] lowercaseString];
            NSArray * typeSubTypeParts = [typeSubType componentsSeparatedByString:@"/"];
            
            [self setType:typeSubTypeParts[0] ];
            if ([typeSubTypeParts count]==2){
                [self setSubtype:typeSubTypeParts[1] ];
            }
            else{
                [self setSubtype:typeSubTypeParts[0]];
            }
            
            static NSMutableCharacterSet * whitespaceQuoteCharSet = nil;
            static NSCharacterSet * whitespaceCharSet = nil;
            static NSData * mimePartDelimiter = nil;
            
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                whitespaceQuoteCharSet = [[NSCharacterSet whitespaceAndNewlineCharacterSet] mutableCopy];
                [whitespaceQuoteCharSet addCharactersInString:@"\""];
                whitespaceCharSet =[NSCharacterSet whitespaceAndNewlineCharacterSet];

                mimePartDelimiter = [@"--" dataUsingEncoding:NSASCIIStringEncoding];
            });
            NSArray * componentsSubParts = [contentTypeHeaderParts subarrayWithRange:NSMakeRange(1, componentCount-1)];
            
            for (NSString *componentSubPart in componentsSubParts) {
                NSString * component = [componentSubPart stringByTrimmingCharactersInSet:whitespaceCharSet];
                
                if ([[component lowercaseString] hasPrefix:@"boundary="]) {
                    NSString * boundary =[[component substringFromIndex:[@"boundary=" length]] stringByTrimmingCharactersInSet:whitespaceQuoteCharSet];
                    NSData *contentData= [mimeData bodySubdata];

                    self.boundary = boundary;
                    
                    NSData * startBoundaryData = [[@"\n--" stringByAppendingString:boundary] dataUsingEncoding:NSASCIIStringEncoding];
                    NSData * boundarydata = [[@"\n" stringByAppendingString:boundary] dataUsingEncoding:NSASCIIStringEncoding];
                    
                    NSArray <NSData*> * mimeComponents = [contentData componentsSeparatedByData:startBoundaryData];
                    
                    if (![mimeComponents count]) {
                        mimeComponents = [contentData componentsSeparatedByData:boundarydata];
                    }
                    
                    NSMutableArray * muSubParts = [NSMutableArray array];
                    for (NSData * mimePartData in mimeComponents) {
                        NSData * trimmedData = [mimePartData dataByTrimmingBytesInCharacterSet: whitespaceCharSet];
                        if ([trimmedData length] && ![trimmedData isEqualToData:mimePartDelimiter]) {
                            MimePart * subPart = [MimePart mimePartForData:trimmedData];
                            if (subPart.type){
                                [subPart setParent:self];
                                [muSubParts addObject:subPart];
                            }
                        }
                    }
                    if ([muSubParts count]){
                        self.subParts = muSubParts;
                    }
                }
                
                if ([[component lowercaseString] hasPrefix:@"charset="]) {
                    [self setCharset:[[component substringFromIndex:[@"charset=" length]] stringByTrimmingCharactersInSet:whitespaceQuoteCharSet]];
                }
            }
        }
        
        if (!self.subParts){
            self.contentData = [mimeData bodySubdata];
        }
	}
	
    return self;
}

- (void)loadAttachments {
}

-(NSString*)htmlEnvelopeWithContents:(NSString*)contentString{
    return [NSString stringWithFormat:@"<html>\n"
                @"<head>\n"
                    @"<style type=\"text/css\">\n"
                        @"pre {\n"
                                @"font-family:\"Helvetica Neue\";\n"
                                @"font-weight:200;\n"
                                @"font-size:14px;\n"
                        @"    }\n"
                        @"body {background-color:#eee;}\n"
                        @".box {margin:10px; border:0.5px solid #ccc; padding:10px; background-color:#fff; box-shadow: 1px 1px 5px #ccc;}"
                    @"</style>\n"
                    @"<script>\n"
                        @"dragout.ondragstart = function(e) {\n"
                            @"var dt = e.dataTransfer,\n"
                            @"data = this.getAttribute(\"data-downloadurl\");\n"
                            @"\n"
                            @"dt.setData(\"DownloadURL\", data);\n"
                        @"}\n"
                    @"</script>\n"
                @"</head>\n"
                @"<body>\n"
                @"<div class=\"box\">\n"
                "%@\n"
                @"</div>\n"
                @"</body>\n"
                @"</html>",contentString];
}

-(NSString*)MIMEtype{
    return [NSString stringWithFormat:@"%@/%@",self.type,self.subtype];
    
}
-(NSData*)base64Data{
    if ([[[self contentTransferEncoding] lowercaseString] isEqualToString:@"base64"]){
        //clean up the data
        return [[[NSData alloc] initWithBase64EncodedData: self.contentData
                                                  options: NSDataBase64DecodingIgnoreUnknownCharacters] base64EncodedDataWithOptions:0];
    }
    else{
        return [self.contentData base64EncodedDataWithOptions:0];
    }
}

-(NSString*) webArchivePlainContent{
    return nil;
}
- (NSString*) webArchiveHtmlContent{
    return nil;
}

-(NSData*) webArchiveData{
    // fallback -- there is content data but no type specified in headers so assume it is plain text
    if (self.contentData && !self.type){
        NSString * contentString = [[NSString alloc] initWithData:self.contentData encoding:self.stringEncoding];
        NSString * htmlString =  [NSString stringWithFormat:@"<pre style=\"white-space: pre-wrap\">%@</pre>\n",contentString];
        return [[self htmlEnvelopeWithContents:htmlString] dataUsingEncoding:NSUTF8StringEncoding];
    }
    return nil;
}


- (NSArray *) webArchiveResources {
    // if a mime email uses cid: links for images, we need to provide the images as web resources so they can be converted to
    // data: URI by the webResourceLoader delegate;
     NSMutableArray * resources = [NSMutableArray array];
    
    if ([self contentId]){
        NSString * contentId = [self.contentId stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
        if ([[[self contentTransferEncoding] lowercaseString] isEqualToString:@"base64"]){
            NSURL * resourceURL = [NSURL URLWithString:[NSString stringWithFormat:@"cid:%@",contentId]];
            NSData * rawData = [[NSData alloc] initWithBase64EncodedData: self.contentData options: NSDataBase64DecodingIgnoreUnknownCharacters];
            WebResource * resource = [[WebResource alloc] initWithData: rawData
                                                                   URL: resourceURL
                                                              MIMEType: [self MIMEtype]
                                                      textEncodingName: @"base64"
                                                             frameName: nil];
            if (resource){
                [resources addObject: resource];
            }
        }
    }
    return resources;
}

- (NSString*) htmlContent{
    return nil;
}

- (NSString*) plainContent{
    return nil;
}

- (NSData*) dataContent{
    return nil;
}

-(NSString *) filename{
    return [self.contentDisposition[@"filename"] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\""]];
}

- (MimePart*)partWithContentID:(NSString*)contentID{
    if ([[self.contentId stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] isEqualToString:contentID]){
        return self;
    }
    return nil;
}

-(NSArray <NSString*> *) attachments{
    return @[];
}


- (NSStringEncoding)stringEncoding {
    if ([self charset]){
        if ([[self charset] caseInsensitiveCompare:kMimeContentEncodingISO8859] == NSOrderedSame) {
            return NSISOLatin1StringEncoding;
        }
        else if ([[self charset] caseInsensitiveCompare:kMimeContentEncodingUTF8] == NSOrderedSame) {
            return NSUTF8StringEncoding;
        }
        else if ([[self charset] caseInsensitiveCompare:kMimeContentEncodingUSASCII] == NSOrderedSame) {
            return NSASCIIStringEncoding;
        }
    }
    return NSASCIIStringEncoding;
}

/* Getter */
- (NSString *)contentTransferEncoding {
	if (self.internalContentTransferEncoding) {
		return self.internalContentTransferEncoding;
	}
	else {
		return [[self parent] contentTransferEncoding];
	}
}

/* Setter */
- (void)setContentTransferEncoding:(NSString *)contentTransferEncoding {
    self.internalContentTransferEncoding = contentTransferEncoding;
}

/* Getter */
- (NSString *)charset {
	if (self.internalCharset) {
		return self.internalCharset;
	}
	else {
		return [[self parent] charset];
	}
}

/* Setter */
- (void)setCharset:(NSString *)charset {
    self.internalCharset = charset;
}

- (NSString *)description {
    return [NSString stringWithFormat: @"<%@ %p> type: %@ subType:%@ encoding: %@\nSubParts:\n%ld",NSStringFromClass([self class]),self,self.type,self.subtype, self.contentTransferEncoding,[self.subParts count]];
}




@end
