//
//  MKCommon.h
//  MKMimeKit
//
//  Created by smorr on 2015-07-04.
//
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//


#import <objc/runtime.h>

#ifndef Martlet_MKCommon_h
#define Martlet_MKCommon_h

#define class(cname) YES?objc_getClass(#cname):0
#define key(cname) YES?sel_registerName(#cname):0


// @format(@"formatString %@",a)
#define format(formatString,...) YES?[NSString stringWithFormat:formatString, ##__VA_ARGS__]:0
#endif

#define localized(str) YES?[NSString stringWithUTF8String:#str]:0

#if __has_feature(objc_generics)

#define _of(className) <className *>
#define _of_to(keyClass,valueClass) <keyClass *, valueClass*>

#else

#define _of(className)
#define _of_to(keyClass,valueClass)
#define _Nonnull

#endif


