//
//  MKMimeHeaders.m
//  MKMimeKit
//
//  Created by Scott Morrison on 11-01-30.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "MKMimeHeaders.h"
#import "NSString+MailKit.h"

#import "MKMimeHeadersParser.h"
#import "NSString+MimeEncoding.h"

@interface MKMimeHeaders()
@property (readwrite,strong) NSDictionary * headerDictionary; /* Retained */

@end

static const NSUInteger kMaxEncodedHeaderLineLength = 75;
static const NSUInteger kMaxHeaderLineLength = 78;

@implementation MKMimeHeaders

// to do
//      headers need to have a preferred encoding -- all headers should follow the same encoding if possible.


+ (NSString*)mimeEncodedHeaderForKey:(NSString*)headerKey 
                               value:(NSString*)string 
                   preferredEncoding:(NSStringEncoding)encoding 
                        encodingUsed:(NSStringEncoding*)usedEncoding{
    
    NSStringEncoding myUsedEncoding = 0;
    NSString * quotedPrintable = [NSString qEncodedStringWithString:string preferredEncoding:encoding encodingUsed:&myUsedEncoding];
    
    if (usedEncoding) *usedEncoding = myUsedEncoding;
    
    if (quotedPrintable && myUsedEncoding){
        NSString * encodingName = [NSString MKcharSetNameForEncoding:myUsedEncoding];
        if (encodingName){
            // string needs to be broken up into different mime words if length exceeds 76 char
            NSMutableArray * lines = [NSMutableArray array];
            NSUInteger curPosition = 0;
            NSUInteger lineStart = 0;
            
            NSString * linePrefix = nil;
            if (headerKey){
                linePrefix = [NSString stringWithFormat:@"%@: =?%@?Q?",[headerKey capitalizedHeaderKey],encodingName]; // first part is for header key
            }
            else{
                linePrefix = [NSString stringWithFormat:@"=?%@?Q?",encodingName]; // note no leading space on first line.
            }
            
            NSString * subsequentLinePrefix = [NSString stringWithFormat:@" =?%@?Q?",encodingName];// note leading space is needed
            NSString * lineSuffix = @"?=";
            
            while( YES ){
                NSInteger currLineLength = linePrefix.length + (curPosition - lineStart) + lineSuffix.length;
                
                if (curPosition < quotedPrintable.length){
                    unichar c = [quotedPrintable characterAtIndex:curPosition]; // ok to use character size of 1 as string should be all 7bit ascii
                    NSInteger maxLengthForCurrentLine = kMaxEncodedHeaderLineLength;
                    if (c == '=') maxLengthForCurrentLine = kMaxEncodedHeaderLineLength - 3 ; // headers line cannot break =xx encoding, so shorten the max length by 3 chars
                    
                    if (currLineLength >= maxLengthForCurrentLine){
                        NSString * currentLine = [NSString stringWithFormat:@"%@%@%@",linePrefix,[quotedPrintable substringWithRange:NSMakeRange(lineStart, (curPosition-lineStart))],lineSuffix];
                        [lines addObject:currentLine];
                        lineStart = curPosition;
                        linePrefix = subsequentLinePrefix;
                    }
                    else{
                        curPosition++;
                    }
                }
                else {
                    NSString * currentLine = [NSString stringWithFormat:@"%@%@?=",linePrefix,[quotedPrintable substringWithRange:NSMakeRange(lineStart, (curPosition - lineStart))]];
                    [lines addObject:currentLine];
                    break;
                }
            }
            return [lines componentsJoinedByString:@"\r\n"];
        }
    }
    return nil;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.headerDictionary = [NSDictionary dictionary];
    }
    return self;
}

- (id)initWithHeaderDictionary:(NSDictionary *)aHeaderDictionary {
    self = [super init];
    
    if (self) {
        self.headerDictionary = aHeaderDictionary;
    }
    
    return self;
}

- (NSString *)firstHeaderForKey:(NSString *)key {
    NSArray * headerKeys = [[self headerDictionary] allKeys];
    for (NSString * aHeaderKey in headerKeys){
        if ([key caseInsensitiveCompare:aHeaderKey] == NSOrderedSame){
            return [[[[self headerDictionary] objectForKey:aHeaderKey] objectAtIndex:0] stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
        }
    }
    return nil;
}

- (NSString *)unfoldedHeaderString {
    NSMutableArray * fields = [NSMutableArray array];
    
    for (NSString * key in [[self headerDictionary] allKeys]) {
        NSString * value = [[self headerDictionary] objectForKey:key] ;
        NSString * encodedValue = nil;
        
        if ([value canBeConvertedToEncoding:NSASCIIStringEncoding]){
            encodedValue = value;
        }
        else{
            encodedValue = [NSString mimeWordWithString:value preferredEncoding:NSISOLatin1StringEncoding encodingUsed:nil];
        }
        
        [fields addObject:[NSString stringWithFormat:@"%@: %@", key, encodedValue]];
    }
    
    return [fields componentsJoinedByString:@"\r\n"];
}

- (NSString *)foldedHeaderString {
    NSMutableArray * fields = [NSMutableArray array];
    NSArray * allKeys = [[[self headerDictionary] allKeys] valueForKey:@"capitalizedHeaderKey"];
    NSArray * preferredKeys = @[@"From",@"To",@"Cc",@"Date",@"Subject",@"Message-Id"];
    NSMutableArray * orderKeys = [NSMutableArray array];
    for (NSString * key in preferredKeys) {
        if ([allKeys containsObject:key]){
            [orderKeys addObject:key];
        }
    }
    for (NSString * key in allKeys) {
        if (![orderKeys containsObject:key]){
            [orderKeys addObject:key];
        }
    }
    
    for (NSString * key in orderKeys) {
        NSString * value = [self firstHeaderForKey:key];
        NSString * encodedValue = nil;
        NSStringEncoding encodingUsed = NSASCIIStringEncoding;
        if ([value canBeConvertedToEncoding:NSASCIIStringEncoding]){
            encodedValue = value;
            NSString * field = [NSString stringWithFormat:@"%@: %@", key , encodedValue];
            if ([field length] > kMaxHeaderLineLength) {
                NSMutableString * foldedField = [NSMutableString string];
                NSScanner * scanner = [NSScanner scannerWithString:field];
                [scanner setCharactersToBeSkipped:nil];
                
                NSInteger lineStartLocation = 0;
                NSInteger lastWhitespaceIndex = 0;
                
                while (![scanner isAtEnd]) {
                    if (![scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:nil]) {
                        //Since there is no more whitespace, move the scanner to the end
                        [scanner setScanLocation:[field length]];
                    }
                    
                    if (([scanner scanLocation] - lineStartLocation) > kMaxHeaderLineLength) { //Lines SHOULD be no longer than kMaxHeaderLineLength + CRLF (80) {
                        NSInteger lineEndLocation;
                        
                        if (lastWhitespaceIndex > lineStartLocation) {
                            //There is whitespace in this section (between line start and scanner location), append until last whitespace
                            lineEndLocation = lastWhitespaceIndex;
                        }
                        else {
                            //No previous whitespace available on this line, append entire string (word longer than kMaxHeaderLineLength characters)
                            lineEndLocation = [scanner scanLocation];
                        }
                        
                        [foldedField appendString:[field substringWithRange:NSMakeRange(lineStartLocation, lineEndLocation - lineStartLocation)]];
                        
                        [scanner setScanLocation:lineEndLocation];
                        
                        if ([scanner isAtEnd]) break;
                        
                        [foldedField appendString:@"\r\n"];
                        lineStartLocation = lineEndLocation;
                    }
                    
                    if ([field length] - lineStartLocation <= kMaxHeaderLineLength) {
                        //If what's left is less than the length restriction, append the rest and break out of the loop
                        [foldedField appendString:[field substringFromIndex:lineStartLocation]];
                        
                        break;
                    }
                    
                    lastWhitespaceIndex = [scanner scanLocation];
                    
                    [scanner scanCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:nil];
                }
                
                [fields addObject:foldedField];
            }
            else {
                [fields addObject:field];
            }
        }
        else{
            NSString * foldedField = [MKMimeHeaders mimeEncodedHeaderForKey:key value:value preferredEncoding:NSISOLatin1StringEncoding encodingUsed:&encodingUsed];
            [fields addObject:foldedField];
         }

    
    }
    if ([fields count]){
        return [fields componentsJoinedByString:@"\r\n"] ;
    }
    return @"";
}

-(void)setHeader:(NSString*)headerValue forKey:(NSString*)headerKey{

    NSString * capitalizedHeaderKey = [headerKey capitalizedHeaderKey];
    
    NSMutableDictionary * muDict = [self.headerDictionary mutableCopy];
    muDict[capitalizedHeaderKey] = @[headerValue];
    
    self.headerDictionary = [NSDictionary dictionaryWithDictionary:muDict];
}

@end
