//
//  MultipartMimePart.m
//  MKMimeKit
//
//  Created by smorr on 2016-01-12.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "MultipartMimePart.h"
#import "MKCommon.h"
#import "MKMimeHeaders.h"

#import <WebKit/WebKit.h>
@interface MultipartMimePart()
@property (assign) BOOL isMutable;
@end

@implementation MultipartMimePart

NSString * const kMimeTypeMultipart = @"multipart";

NSString * const kMimeMultipartSubtypeRelated = @"related";
NSString * const kMimeMultipartSubtypeAternative = @"alternative";
NSString * const kMimeMultipartSubtypeMixed = @"mixed";

NSString * const kMimeMultipartBoundaryFormatString = @"MKMimeKit=_%@";

+(void)load{
    [self registerForMimeType:@"multipart/*"];
}

+(NSString*)newBoundary{
    return @format(kMimeMultipartBoundaryFormatString,[[NSUUID UUID] UUIDString]);
}

-(instancetype)initWithSubtype:(NSString*)subtype {
    self = [super init];
    if (self){
        self.type = kMimeTypeMultipart;
        self.subtype = subtype;
        self.isMutable = YES;
        self.boundary = [MultipartMimePart newBoundary];
        self.subParts = [NSMutableArray array];
        self.headers = [[MKMimeHeaders alloc] init];
        [self.headers setHeader: @format(@"%@/%@;\r\n boundary=\"%@\"",self.type,self.subtype,self.boundary) forKey:@"Content-Type"];
    }
    return self;
}

-(void)addPart:(MimePart*)part{
    if (!self.isMutable){
        NSLog (@"Cannot Mutate Multipart part");
        return;
    }
    
    if (![self.subParts containsObject: part]){
        [self.subParts addObject:part];
        NSMutableData * contentData =[NSMutableData data];
        [self.subParts enumerateObjectsUsingBlock:^(MimePart  * aSubpart, NSUInteger idx, BOOL * _Nonnull stop) {
            [contentData appendData: [@format(@"%@--%@\r\n",idx==0?@"":@"\r\n",self.boundary) dataUsingEncoding:NSASCIIStringEncoding]];
            [contentData appendData: [@format(@"%@\r\n\r\n",[aSubpart.headers foldedHeaderString]) dataUsingEncoding:NSASCIIStringEncoding]];
            [contentData appendData: aSubpart.contentData];
        }];
        [contentData appendData: [@format(@"\r\n--%@--\r\n",self.boundary) dataUsingEncoding:NSASCIIStringEncoding]];
        self.contentData = contentData;
    }
}

-(instancetype)initWithMimeData:(NSData *)mimeData{
    self = [super initWithMimeData:mimeData];
    if (self){
        self.isMutable = NO;
    }
    return self;
}

-(NSString*) webArchivePlainContent{
    NSMutableArray * plainParts = [NSMutableArray array];
    for (MimePart * aPart in [self subParts]) {
        NSString * plainPart = [aPart webArchivePlainContent];
        if (plainPart){
            [plainParts addObject:plainPart];
        }
    }
    if ([plainParts count]){
        return  [plainParts componentsJoinedByString:@""];
    }
    return nil;
}

-(NSString*) webArchiveHtmlContent{
    NSMutableArray * htmlParts = [NSMutableArray array];
    for (MimePart * aPart in [self subParts]) {
        NSString * html = [aPart webArchiveHtmlContent];
        if (html){
            [htmlParts addObject:html];
            //break;
        }
    }
    if ([htmlParts count]){
        return  [htmlParts componentsJoinedByString:@""];
    }
    return nil;
}
-(NSArray <NSString*> *) attachments{
    NSMutableArray * attachments = [NSMutableArray array];
    for (MimePart * aPart in [self subParts]) {
       [attachments addObjectsFromArray: [aPart attachments]];
     }
    return attachments;
}

-(NSData*) webArchiveData{
    NSMutableArray * contentParts = [NSMutableArray array];
    for (MimePart * aPart in [self subParts]) {
        NSString * html = [aPart webArchiveHtmlContent];
        if (!html) html = [aPart webArchivePlainContent];
        if (html){
            [contentParts addObject:html];
        }
    }
    if ([contentParts count]){
        return  [[self htmlEnvelopeWithContents:[contentParts componentsJoinedByString:@""]] dataUsingEncoding:NSUTF8StringEncoding];
    }
    return nil;
}


- (NSArray <WebResource*> *) webArchiveResources{
    NSMutableArray * resources = [NSMutableArray array];
    for (MimePart * apart in [self subParts]){
        [resources addObjectsFromArray:[apart webArchiveResources]];
    }
    return resources;
}


- (NSString*) htmlContent{
    NSMutableArray * displayComponents = [NSMutableArray array];
    for (MimePart * aPart in [self subParts]) {
        NSString * partHtmlContent = [aPart htmlContent];
        if (partHtmlContent) {
            [displayComponents addObject:partHtmlContent];
        }
    }
    if ([displayComponents count]){
        return [displayComponents componentsJoinedByString:@""];
    }
    return nil;
}


-(NSString *)plainContent{
    NSMutableArray * displayComponents = [NSMutableArray array];
    
    for (MimePart * aPart in [self subParts]) {
        NSString * plainContent = [aPart plainContent];
        if (plainContent) {
            [displayComponents addObject:plainContent];
        }
    }
    if ([displayComponents count]){
        return [displayComponents componentsJoinedByString:@""];
    }
    return nil;
}

- (MimePart*)partWithContentID:(NSString*)contentID{
    MimePart * foundPart = nil;
    for (MimePart * aPart in [self subParts]) {
        foundPart = [aPart partWithContentID:contentID];
        if (foundPart) {
            return foundPart;
        }
    }
    return nil;
}

@end
