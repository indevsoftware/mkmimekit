//
//  MultipartRelatedMimePart.m
//  Martlet
//
//  Created by smorr on 10-03-2016.
//
//

#import "MultipartRelatedMimePart.h"
#import "MessageHeaders.h"

@implementation MultipartRelatedMimePart
-(instancetype)initWithHtmlPart:(MimePart*) htmlPart relatedParts:(NSArray _of(MimePart)*)relatedParts{
    self = [super initWithSubtype:kMimeMultipartSubtypeRelated];
    if (self){
        [self addPart:htmlPart];
        for (MimePart * aPart in relatedParts){
             [self addPart:aPart];
        }
        [self.headers setHeader: @format(@"%@/%@;\r\n type=\"%@\";\r\n boundary=\"%@\"",self.type,self.subtype,@"text/html",self.boundary) forKey:kMimeHeaderContentType];
    }
    return self;

}


@end
