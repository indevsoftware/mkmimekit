//
//  NSData+Base64.m
//  base64
//
//  Created by Matt Gallagher on 2009/06/03.
//  Copyright 2009 Matt Gallagher. All rights reserved.
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software. Permission is granted to anyone to
//  use this software for any purpose, including commercial applications, and to
//  alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source
//     distribution.
//

#import "NSData+Base64.h"
#import "NSString+HTMLEntities.h"
//
// Mapping from 6 bit pattern to ASCII character.
//
static unsigned char base64EncodeLookup[65] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//
// Definition for "masked-out" areas of the base64DecodeLookup mapping
//
#define xx 65

//
// Mapping from ASCII character to 6 bit pattern.
//
static unsigned char base64DecodeLookup[256] = {
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 62, xx, xx, xx, 63, 
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, xx, xx, xx, xx, xx, xx, 
    xx,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, xx, xx, xx, xx, xx, 
    xx, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
};

//
// Fundamental sizes of the binary and base64 encode/decode units in bytes
//
#define BINARY_UNIT_SIZE 3
#define BASE64_UNIT_SIZE 4

//
// NewBase64Decode
//
// Decodes the base64 ASCII string in the inputBuffer to a newly malloced
// output buffer.
//
//  inputBuffer - the source ASCII string for the decode
//	length - the length of the string or -1 (to specify strlen should be used)
//	outputLength - if not-NULL, on output will contain the decoded length
//
// returns the decoded buffer. Must be free'd by caller. Length is given by
//	outputLength.
//
void *NewBase64Decode(
	const char *inputBuffer,
	size_t length,
	size_t *outputLength) {
	if (length == -1) {
		length = strlen(inputBuffer);
	}
	
	size_t outputBufferSize =
		((length+BASE64_UNIT_SIZE-1) / BASE64_UNIT_SIZE) * BINARY_UNIT_SIZE;
	unsigned char *outputBuffer = (unsigned char *)malloc(outputBufferSize);
	
	size_t i = 0;
	size_t j = 0;
	while (i < length) {
		//
		// Accumulate 4 valid characters (ignore everything else)
		//
		unsigned char accumulated[BASE64_UNIT_SIZE];
		size_t accumulateIndex = 0;
		while (i < length) {
			unsigned char decode = base64DecodeLookup[inputBuffer[i++]];
			if (decode != xx) {
				accumulated[accumulateIndex] = decode;
				accumulateIndex++;
				
				if (accumulateIndex == BASE64_UNIT_SIZE) {
					break;
				}
			}
		}
		
		//
		// Store the 6 bits from each of the 4 characters as 3 bytes
		//
		// (Uses improved bounds checking suggested by Alexandre Colucci)
		//
		if(accumulateIndex >= 2)  
			outputBuffer[j] = (accumulated[0] << 2) | (accumulated[1] >> 4);  
		if(accumulateIndex >= 3)  
			outputBuffer[j + 1] = (accumulated[1] << 4) | (accumulated[2] >> 2);  
		if(accumulateIndex >= 4)  
			outputBuffer[j + 2] = (accumulated[2] << 6) | accumulated[3];
		j += accumulateIndex - 1;
	}
	
	if (outputLength) {
		*outputLength = j;
	}
	return outputBuffer;
}

//
// NewBase64Encode
//
// Encodes the arbitrary data in the inputBuffer as base64 into a newly malloced
// output buffer.
//
//  inputBuffer - the source data for the encode
//	length - the length of the input in bytes
//  separateLines - if zero, no CR/LF characters will be added. Otherwise
//		a CR/LF pair will be added every 64 encoded chars.
//	outputLength - if not-NULL, on output will contain the encoded length
//		(not including terminating 0 char)
//
// returns the encoded buffer. Must be free'd by caller. Length is given by
//	outputLength.
//
char *NewBase64Encode(
	const void *buffer,
	size_t length,
	bool separateLines,
	size_t *outputLength) {
	const unsigned char *inputBuffer = (const unsigned char *)buffer;
	
	#define MAX_NUM_PADDING_CHARS 2
	#define OUTPUT_LINE_LENGTH 64
	#define INPUT_LINE_LENGTH ((OUTPUT_LINE_LENGTH / BASE64_UNIT_SIZE) * BINARY_UNIT_SIZE)
	#define CR_LF_SIZE 2
	
	//
	// Byte accurate calculation of final buffer size
	//
	size_t outputBufferSize =
			((length / BINARY_UNIT_SIZE)
				+ ((length % BINARY_UNIT_SIZE) ? 1 : 0))
					* BASE64_UNIT_SIZE;
	if (separateLines) {
		outputBufferSize +=
			(outputBufferSize / OUTPUT_LINE_LENGTH) * CR_LF_SIZE;
	}
	
	//
	// Include space for a terminating zero
	//
	outputBufferSize += 1;

	//
	// Allocate the output buffer
	//
	char *outputBuffer = (char *)malloc(outputBufferSize);
	if (!outputBuffer) {
		return NULL;
	}

	size_t i = 0;
	size_t j = 0;
	const size_t lineLength = separateLines ? INPUT_LINE_LENGTH : length;
	size_t lineEnd = lineLength;
	
	while (true) {
		if (lineEnd > length) {
			lineEnd = length;
		}

		for (; i + BINARY_UNIT_SIZE - 1 < lineEnd; i += BINARY_UNIT_SIZE) {
			//
			// Inner loop: turn 48 bytes into 64 base64 characters
			//
			outputBuffer[j++] = base64EncodeLookup[(inputBuffer[i] & 0xFC) >> 2];
			outputBuffer[j++] = base64EncodeLookup[((inputBuffer[i] & 0x03) << 4)
				| ((inputBuffer[i + 1] & 0xF0) >> 4)];
			outputBuffer[j++] = base64EncodeLookup[((inputBuffer[i + 1] & 0x0F) << 2)
				| ((inputBuffer[i + 2] & 0xC0) >> 6)];
			outputBuffer[j++] = base64EncodeLookup[inputBuffer[i + 2] & 0x3F];
		}
		
		if (lineEnd == length) {
			break;
		}
		
		//
		// Add the newline
		//
		outputBuffer[j++] = '\r';
		outputBuffer[j++] = '\n';
		lineEnd += lineLength;
	}
	
	if (i + 1 < length) {
		//
		// Handle the single '=' case
		//
		outputBuffer[j++] = base64EncodeLookup[(inputBuffer[i] & 0xFC) >> 2];
		outputBuffer[j++] = base64EncodeLookup[((inputBuffer[i] & 0x03) << 4)
			| ((inputBuffer[i + 1] & 0xF0) >> 4)];
		outputBuffer[j++] = base64EncodeLookup[(inputBuffer[i + 1] & 0x0F) << 2];
		outputBuffer[j++] =	'=';
	}
	else if (i < length) {
		//
		// Handle the double '=' case
		//
		outputBuffer[j++] = base64EncodeLookup[(inputBuffer[i] & 0xFC) >> 2];
		outputBuffer[j++] = base64EncodeLookup[(inputBuffer[i] & 0x03) << 4];
		outputBuffer[j++] = '=';
		outputBuffer[j++] = '=';
	}
	outputBuffer[j] = 0;
	
	//
	// Set the output length and return the buffer
	//
	if (outputLength) {
		*outputLength = j;
	}
	return outputBuffer;
}

@implementation NSData (Base64)

//
// dataFromBase64String:
//
// Creates an NSData object containing the base64 decoded representation of
// the base64 string 'aString'
//
// Parameters:
//    aString - the base64 string to decode
//
// returns the autoreleased NSData representation of the base64 string
//
+ (NSData *)dataFromBase64String:(NSString *)aString {
	NSData *data = [aString dataUsingEncoding:NSASCIIStringEncoding];
	size_t outputLength;
	void *outputBuffer = NewBase64Decode([data bytes], [data length], &outputLength);
	NSData *result = [NSData dataWithBytes:outputBuffer length:outputLength];
	free(outputBuffer);
    return result;
}

//
// base64EncodedString
//
// Creates an NSString object that contains the base 64 encoding of the
// receiver's data. Lines are broken at 64 characters long.
//
// returns an autoreleased NSString being the base 64 representation of the
//	receiver.
//
- (NSString *)MKbase64EncodedStringWithOptions:(NSDataBase64EncodingOptions)options {
    NSString *result = nil;
	size_t outputLength = 0;
	char *outputBuffer = NewBase64Encode([self bytes], [self length], false, &outputLength);
    if (outputBuffer){
        result = [[NSString alloc] initWithBytes:outputBuffer length:outputLength encoding:NSASCIIStringEncoding];
        free(outputBuffer);
    }
	return result;
}

- (NSString *)pathSafeBase64EncodedString {
    
    if ([self respondsToSelector:@selector(base64EncodedStringWithOptions:)]){
        NSString    *cleaned = [[self base64EncodedStringWithOptions:0] stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        cleaned = [cleaned stringByReplacingOccurrencesOfString:@"+" withString:@"_"];
        return cleaned;
    }
    else{
        const char* input =  [self bytes];
        NSInteger length = [self length];
        
        static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_=";
        
        NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
        uint8_t* output = (uint8_t*)data.mutableBytes;
        
        NSInteger i;
        for (i=0; i < length; i += 3) {
            NSInteger value = 0;
            NSInteger j;
            for (j = i; j < (i + 3); j++) {
                value <<= 8;
                if (j < length) {
                    value |= (0xFF & input[j]);
                }
            }
            
            NSInteger theIndex = (i / 3) * 4;
            output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
            output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
            output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
            output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
        }
        return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ;
    }
}

-(NSString*)stringFromQuotedPrintableStringWithEncoding:(NSStringEncoding)encoding{
    NSString * contentString = [[NSString alloc] initWithData:self encoding:encoding];
    NSString * c1 = [[contentString stringByReplacingOccurrencesOfString:@"=\r\n" withString:@""] stringByReplacingOccurrencesOfString:@"=\r" withString:@""];
    NSString * c2 = [[c1 stringByDecodingHTMLEntities] stringByReplacingOccurrencesOfString:@"%" withString:@"%25"];
    NSString * c3 = [c2 stringByReplacingOccurrencesOfString:@"=" withString:@"%"];
    return c3;
    
}
-(NSString*)htmlEncodedStringFromQuotedPrintableStringWithEncoding:(NSStringEncoding)encoding{
    NSString * contentString = [[NSString alloc] initWithData:self encoding:encoding];
    NSString * c1 = [[contentString stringByReplacingOccurrencesOfString:@"=\r\n" withString:@""] stringByReplacingOccurrencesOfString:@"=\r" withString:@""];
    NSString * c2 = [c1 stringByReplacingOccurrencesOfString:@"%" withString:@"%25"];
    //NSString * c2 = [[c1 stringByDecodingHTMLEntities] stringByReplacingOccurrencesOfString:@"%" withString:@"%25"];
    NSString * c3 = [c2 stringByReplacingOccurrencesOfString:@"=" withString:@"%"];
    NSString * c4 = [c3 stringByReplacingPercentEscapesUsingEncoding:encoding];
    NSString * c5 = [c4 stringByEscapingForHTMLContent];
   // NSString * c5 = c4;
    return c5;
}


-(NSString*)decodedQuotedPrintableStringWithEncoding:(NSStringEncoding)encoding{
    NSString * contentString = [[NSString alloc] initWithData:self encoding:encoding];
    NSString * c1 = [[contentString stringByReplacingOccurrencesOfString:@"=\r\n" withString:@""] stringByReplacingOccurrencesOfString:@"=\r" withString:@""];
    NSString * c2 = [c1 stringByReplacingOccurrencesOfString:@"%" withString:@"%25"];
    NSString * c3 = [c2 stringByReplacingOccurrencesOfString:@"=" withString:@"%"];
    NSString * c4 = [c3 stringByReplacingPercentEscapesUsingEncoding:encoding];
    NSString * c5 = [c4 stringByEscapingForAsciiHTMLContent];
    return c5;
}


-(NSString*) stringByConvertingLiteralsToBase64{
    static NSData * crlfData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        crlfData = [[NSData alloc] initWithBytes:"\r\n" length:2];
    });
    
    

    if ([self length]<[crlfData length]) return nil;
    BOOL stop = NO;
    NSUInteger lineStartLocation = 0;
    NSUInteger length = [self length];
    NSRange scanRange = NSMakeRange(0,length);
    NSRange crlfRange;
    NSMutableArray * parts = [NSMutableArray array];
    while(lineStartLocation<length && !stop) {
        
        crlfRange = [self rangeOfData:crlfData options:0 range:scanRange];
        NSUInteger lineEnd =crlfRange.location;
        if (lineEnd!=NSNotFound  && lineEnd>0){
            // see if the preceeding character is a '}', indicating that a potential literal response (
            char aByte[1] = {'\0'};
            NSInteger lastLineCharLocation = lineEnd-1;
            [self getBytes:aByte range:NSMakeRange(lastLineCharLocation, 1)];
            if (aByte[0] == '}'){
                {   //Comment
                    // LiteralData
                    // As this data may be encoded in different string encodings we cannot merely decode it using
                    //        -[NSString initWithData: encoding:NSUTF8StringEncoding]
                    // and add it to the response string.
                    // but adding it to a response string we must - or convert the whole parsing mechanism to parse NSData objects (fuck that)
                    //
                    // Solution is to capture the subdata of the literal length indicated and encode it as a base64 string. (which will be
                    // NSASCIIStringEncoding -- something that can be appended to the response line.
                    // in doing so, we change the literal length to be the length of the base64 encoded string;
                    //
                    // This offloads any string encoding issues to later when we can read the headers and we can determine the encode
                    // or we can write the raw data to file :)
                    // for example
                    /* the response:
                     * 443 FETCH (UID 676 BODY[] {454}
                     Content-Type: text/plain;
                     charset=us-ascii
                     Subject: 23-8
                     Mime-Version: 1.0 (Mac OS X Mail 7.2 \(1859\))
                     X-Universally-Unique-Identifier: B29CC4E2-9A5A-4004-9950-AD5DA9737851
                     From: Scott Morrison <smorr@me.com>
                     Date: Mon, 23 Dec 2013 13:55:07 -0600
                     Content-Transfer-Encoding: 7bit
                     X-Smtp-Server: smtp://smorr@p01-smtp.mail.me.com
                     Message-Id: <8F641793-F409-46E1-9900-EAABA4DCD9DF@me.com>
                     To: Scott Morrison <mailtags4@gmail.com>
                     
                     dsfsdfsfsf
                     )
                     
                     becomes:
                     * 443 FETCH (UID 676 BODY[] {608}¬
                     Q29udGVudC1UeXBlOiB0ZXh0L3BsYWluOw0KCWNoYXJzZXQ9dXMtYXNjaWkNClN1YmplY3Q6IDIzLTgNCk1pbWUtVmVyc2lvbjo
                     gMS4wIChNYWMgT1MgWCBNYWlsIDcuMiBcKDE4NTlcKSkNClgtVW5pdmVyc2FsbHktVW5pcXVlLUlkZW50aWZpZXI6IEIyOUNDNE
                     UyLTlBNUEtNDAwNC05OTUwLUFENURBOTczNzg1MQ0KRnJvbTogU2NvdHQgTW9ycmlzb24gPHNtb3JyQG1lLmNvbT4NCkRhdGU6I
                     E1vbiwgMjMgRGVjIDIwMTMgMTM6NTU6MDcgLTA2MDANCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDdiaXQNClgtU210cC1T
                     ZXJ2ZXI6IHNtdHA6Ly9zbW9yckBwMDEtc210cC5tYWlsLm1lLmNvbQ0KTWVzc2FnZS1JZDogPDhGNjQxNzkzLUY0MDktNDZFMS0
                     5OTAwLUVBQUJBNERDRDlERkBtZS5jb20+DQpUbzogU2NvdHQgTW9ycmlzb24gPG1haWx0YWdzNEBnbWFpbC5jb20+DQoNCmRzZn
                     NkZnNmc2YNCg==)
                     */
                }
                
                
                NSUInteger numBytes = 0;
                NSUInteger sizeStart = NSNotFound;
                NSInteger byteLocation = lastLineCharLocation;
                while (byteLocation--){
                    [self getBytes:aByte range:NSMakeRange(byteLocation, 1)];
                    if (aByte[0] == '{'){
                        sizeStart = byteLocation;
                        break;
                    }
                    numBytes++;
                }
                if (sizeStart==NSNotFound){
                    // WTF a literal is ended but not started (eg "123}CRLF" )
                    // data is malformed -- return no response lines.
                    stop = YES;
                    continue;
                }
                
                char* sizeString = calloc(numBytes+1, sizeof(char));
                [self getBytes: sizeString range:NSMakeRange(sizeStart+1,numBytes)];
                long literalSize = atol(sizeString);
                free(sizeString);
                
                NSData * subdata = [self subdataWithRange:NSMakeRange(lineStartLocation,sizeStart - lineStartLocation)];
                 NSString * nonLiteralString= [[NSString alloc] initWithData:subdata encoding:NSASCIIStringEncoding];
                [parts addObject:nonLiteralString];
                
                if (lineEnd+2+literalSize>length){
                    stop = YES;
                    continue;
                }
                
                
                NSData * literalData = [self subdataWithRange:NSMakeRange(crlfRange.location+2, literalSize)];
                NSString * literalBase64String = [literalData MKbase64EncodedStringWithOptions:0];
                 [parts addObject: [NSString stringWithFormat:@"{%ld}\r\n%@",[literalBase64String length],literalBase64String]];
                
                scanRange.location = lineEnd+2+literalSize;
                //lineStartLocation = scanRange.location;
                if (scanRange.location>length){
                    // not enough data is found.
                    return nil;
                }
                scanRange.length = length-scanRange.location;
                // loop to extend the subdata to the next CRLF
                continue;
            }
            else{
                NSData * subdata = [self subdataWithRange:NSMakeRange(lineStartLocation,lineEnd - lineStartLocation)];
                NSString * nonLiteralString = [[NSString alloc] initWithData:subdata encoding:NSASCIIStringEncoding];
                [parts addObject:nonLiteralString];
            }
        }
        if (lineEnd == NSNotFound){
            NSData * subdata = [self subdataWithRange:scanRange];
            NSString * nonLiteralString= [[NSString alloc] initWithData:subdata encoding:NSASCIIStringEncoding];
            [parts addObject:nonLiteralString];
            break;
        }
    }

    if (stop){
        return nil;
    }
    return [parts componentsJoinedByString:@""];
    
}
@end
