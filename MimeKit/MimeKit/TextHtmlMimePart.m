//
//  TextHtmlMimePart.m
//  MKMimeKit
//
//  Created by smorr on 2016-01-13.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "TextHtmlMimePart.h"
#import "MKCommon.h"

#import "NSData+Base64.h"
#import "NSString+MimeEncoding.h"
#import "NSString+HTMLEntities.h"
#import <WebKit/WebKit.h>
#import "MKMimeHeaders.h"

@implementation TextHtmlMimePart
+(void)load{
    [self registerForMimeType:@"text/html"];
}


-(instancetype)initWithHtmlString:(NSString* __nonnull) htmlString
                  stringEncoding:(NSStringEncoding)stringEncoding
         contentTransferEncoding:(NSString* __nonnull)transferEncoding{
    self = [super init];
    if (self){
        self.type=@"text";
        self.subtype=@"html";
        self.contentTransferEncoding = transferEncoding;
         
        self.headers = [[MKMimeHeaders alloc] init];
        
      
        
        if ([htmlString canBeConvertedToEncoding:NSASCIIStringEncoding]){
            [self.headers setHeader: kMimeTransferEncoding7Bit forKey:kMimeHeaderContentTransferEncoding];
            self.charset = kMimeTransferEncoding7Bit;
            [self.headers setHeader: @format(@"%@/%@; charset=%@",self.type,self.subtype,self.charset) forKey:kMimeHeaderContentType];
            self.contentData=[htmlString dataUsingEncoding:NSASCIIStringEncoding];
        }
        else{
            [self.headers setHeader: kMimeTransferEncodingQuotedPrintable forKey:kMimeHeaderContentTransferEncoding];
            
            NSStringEncoding encodingUsed = 0;
            NSString * quotedPrintableString = [NSString quotedPrintableStringForPlainTextBody:htmlString preferredEncoding:stringEncoding encodingUsed:&encodingUsed];
            if (encodingUsed == NSASCIIStringEncoding){
                self.charset = kMimeTransferEncoding7Bit;
            }
            else{
                self.charset = [NSString MKcharSetNameForEncoding:encodingUsed];
            }
            [self.headers setHeader: @format(@"%@/%@; charset=%@",self.type,self.subtype,self.charset) forKey:kMimeHeaderContentType];
            self.contentData=[quotedPrintableString dataUsingEncoding:NSASCIIStringEncoding];
        }
        
    }
    return self;
}


-(NSString*) webArchivePlainContent{
    return  nil;
}
-(NSString*) webArchiveHtmlContent{
    return self.htmlContent;
}

-(NSData*) webArchiveData{
    NSString * mainResourceHtmlString = self.webArchiveHtmlContent;
    if (!mainResourceHtmlString){
        mainResourceHtmlString = [self htmlEnvelopeWithContents:self.webArchivePlainContent];
    }
    if ([mainResourceHtmlString length]){
        return [mainResourceHtmlString dataUsingEncoding:NSUTF8StringEncoding];
    }
    return nil;
}


- (NSString*) htmlContent{
    if ([self contentTransferEncoding]){
        if ([[self contentTransferEncoding] caseInsensitiveCompare:kMimeTransferEncodingQuotedPrintable] == NSOrderedSame) {
            return [self.contentData htmlEncodedStringFromQuotedPrintableStringWithEncoding:self.stringEncoding];
        }
        else if ([[self contentTransferEncoding] caseInsensitiveCompare:kMimeTransferEncoding7Bit] == NSOrderedSame) {
            NSString * contentString = [[NSString alloc] initWithData:self.contentData encoding:self.stringEncoding];
            return contentString;
        }
        else if ([[self contentTransferEncoding] caseInsensitiveCompare:kMimeTransferEncodingBase64] == NSOrderedSame) {
            NSData * decodedBase64Data = [[NSData alloc] initWithBase64EncodedData:self.contentData options:NSDataBase64DecodingIgnoreUnknownCharacters];
            NSString * contentString = [[NSString alloc] initWithData:decodedBase64Data encoding:self.stringEncoding];
            return contentString;
        }
    }
    
    NSString * contentString = [[NSString alloc] initWithData:self.contentData encoding:self.stringEncoding];
    return contentString;
}

-(NSString*)plainContent{
    return nil;
}

@end
