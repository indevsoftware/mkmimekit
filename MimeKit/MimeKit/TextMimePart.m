//
//  htmlMimePart.m
//  MKMimeKit
//
//  Created by smorr on 2016-01-12.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "TextMimePart.h"
#import "MKCommon.h"
#import "NSData+Base64.h"
#import <WebKit/WebKit.h>
#import "NSString+HTMLEntities.h"
#import "NSString+MimeEncoding.h"
#import "MKMimeHeaders.h"

@implementation TextMimePart

+(void)load{
    [self registerForMimeType:@"text/*"];
}

-(instancetype)initWithPlainText:(NSString* __nonnull) plainText
                  stringEncoding:(NSStringEncoding)stringEncoding
         contentTransferEncoding:(NSString* __nonnull)transferEncoding{
    self = [super init];
    if (self){
        self.type=@"text";
        self.subtype=@"plain";
        self.contentTransferEncoding = transferEncoding;

        self.headers = [[MKMimeHeaders alloc] init];
    
        if ([plainText canBeConvertedToEncoding:NSASCIIStringEncoding]){
            [self.headers setHeader: kMimeTransferEncoding7Bit forKey:kMimeHeaderContentTransferEncoding];
            self.charset = kMimeTransferEncoding7Bit;
            [self.headers setHeader: @format(@"%@/%@; charset=%@",self.type,self.subtype,self.charset) forKey:kMimeHeaderContentType];
            self.contentData=[plainText dataUsingEncoding:NSASCIIStringEncoding];
        }
        else {
            [self.headers setHeader: kMimeTransferEncodingQuotedPrintable forKey:kMimeHeaderContentTransferEncoding];
              self.charset = (NSString*)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(stringEncoding));
            [self.headers setHeader: @format(@"%@/%@; charset=%@",self.type,self.subtype,self.charset) forKey:kMimeHeaderContentType];
            
            NSStringEncoding encodingUsed = 0;
            NSString * quotedPrintableString = [NSString quotedPrintableStringForPlainTextBody:plainText preferredEncoding:stringEncoding encodingUsed:&encodingUsed];
            self.charset = [NSString MKcharSetNameForEncoding:encodingUsed];
            
            self.contentData=[quotedPrintableString dataUsingEncoding:NSUTF8StringEncoding];
        }
    }
    return self;
}

-(instancetype)initWithMimeData:(NSData *)mimeData{
    self = [super initWithMimeData:mimeData];
    if (self){
        
    }
    return self;
}
-(NSString*) webArchivePlainContent{
    if (self.plainContent){
        NSString * html= [NSString stringWithFormat:@"<pre style=\"white-space: pre-wrap\">%@</pre>\n",[self.plainContent stringByEscapingForAsciiHTMLContent]];
        return html;
    }
    return  nil;
}
-(NSString*) webArchiveHtmlContent{
    return  nil;
}

-(NSData*) webArchiveData{
    NSString * mainResourceHtmlString = self.webArchiveHtmlContent;
    if (!mainResourceHtmlString){
        mainResourceHtmlString = [self htmlEnvelopeWithContents:self.webArchivePlainContent];
    }
    if ([mainResourceHtmlString length]){
        return [mainResourceHtmlString dataUsingEncoding:NSUTF8StringEncoding];
    }
    return nil;
}


- (NSString*) htmlContent{
    return nil;
}

-(NSString*)plainContent{
    if ([self.subtype isEqualToString:@"plain"]) {
        if ([self contentTransferEncoding]){
            if ([[self contentTransferEncoding] caseInsensitiveCompare:kMimeTransferEncodingQuotedPrintable] == NSOrderedSame) {
                return [self.contentData decodedQuotedPrintableStringWithEncoding:self.stringEncoding];
            }
            else if ([[self contentTransferEncoding] caseInsensitiveCompare:kMimeTransferEncoding7Bit] == NSOrderedSame) {
                NSString * contentString = [[NSString alloc] initWithData:self.contentData encoding:self.stringEncoding];
                return contentString;
            }
            else if ([[self contentTransferEncoding] caseInsensitiveCompare:kMimeTransferEncodingBase64] == NSOrderedSame) {
                NSData * decodedBase64Data = [[NSData alloc] initWithBase64EncodedData:self.contentData options:NSDataBase64DecodingIgnoreUnknownCharacters];
                NSString * contentString = [[NSString alloc] initWithData:decodedBase64Data encoding:self.stringEncoding];
                return contentString;
            }
            else if ([[self contentTransferEncoding] caseInsensitiveCompare:kMimeTransferEncoding8Bit] == NSOrderedSame) {
                NSString * contentString = [[NSString alloc] initWithData:self.contentData encoding:self.stringEncoding];
                return contentString;
            }
        }
        NSString * contentString = [[NSString alloc] initWithData:self.contentData encoding:self.stringEncoding];
        return contentString;
    }
    return nil;
}
@end
