//
//  NSString+HTMLEntities.h
//  MKMimeKit
//
//  Created by smorr on 2016-01-01.
//
//

#import <Foundation/Foundation.h>

@interface NSString (HTMLEntities)
- (NSString *)stringByEscapingForHTMLContent;
- (NSString *)stringByEscapingForAsciiHTMLContent;
- (NSString *)stringByDecodingHTMLEntities;
@end
