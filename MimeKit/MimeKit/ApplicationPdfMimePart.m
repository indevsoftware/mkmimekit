//
//  ApplicationPdfMimePart.m
//  MKMimeKit
//
//  Created by smorr on 2016-01-13.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "ApplicationPdfMimePart.h"
#import <WebKit/WebKit.h>
#import "MKMimeHeaders.h"
#import "MKCommon.h"

@interface ApplicationPdfMimePart ()
@property (strong) WebResource* internalWebResource;
@property (strong) NSString* internalWebArchiveHtmlContent;

@end

@implementation ApplicationPdfMimePart
+(void)load{
    [self registerForMimeType:@"application/pdf"];
}

-(instancetype)initWithPathToFile:(NSString*)path inlineId:(NSString*)inlineId{
    self = [super init];
    if (self){
        self.type = @"application";
        self.subtype = @"pdf";
        
        NSString * fileName = [path lastPathComponent];
        NSData * fileContents = [NSData dataWithContentsOfFile:path];
        NSData * base64Contents = [fileContents base64EncodedDataWithOptions:NSDataBase64Encoding76CharacterLineLength];
        
        self.headers = [[MKMimeHeaders alloc] init];
        if (inlineId){
            [self.headers setHeader:@format(@"inline; filename=\"%@\"",fileName) forKey:kMimeHeaderContentDisposition];
            [self.headers setHeader:@format(@"<%@>",inlineId) forKey:kMimeHeaderContentID];
        }
        else{
            [self.headers setHeader:@format(@"attachment; filename=\"%@\"",fileName) forKey:kMimeHeaderContentDisposition];
        }
       
        [self.headers setHeader:@format(@"%@/%@; name=\"%@\"",self.type,self.subtype,fileName) forKey:kMimeHeaderContentType];
        [self.headers setHeader:@"base64" forKey:kMimeHeaderContentTransferEncoding];
        
        self.contentData = base64Contents;
        
    }
    return self;
}

-(instancetype)initWithMimeData:(NSData *)mimeData{
    self = [super initWithMimeData:mimeData];
    if (self){
        if (!self.contentId){
            self.contentId = [[NSUUID UUID] UUIDString];
        }
        
    }
    return self;
}


-(NSArray <NSString*> *) attachments{
    if ([self.contentDisposition[@"type"] isEqualToString:@"attachment"]){
        NSString * fileName = [self.contentDisposition[@"filename"] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\""]];
        if (fileName){
            return @[fileName];
        }
    }
    return nil;
}

-(void) prepareWebResources{
    if (!self.internalWebResource){
        if ([[[self contentTransferEncoding] lowercaseString] isEqualToString:@"base64"]){
            
            NSData * pdfData = [[NSData alloc] initWithBase64EncodedData: self.contentData
                                                                 options: NSDataBase64DecodingIgnoreUnknownCharacters];
            
            NSPDFImageRep *pdfRep = [NSPDFImageRep imageRepWithData:pdfData];
            
            NSString * contentID = [self.contentId stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
            
            int count = [pdfRep pageCount];
            if (count ==1){
                [pdfRep setCurrentPage:0];
                
                NSImage *temp = [[NSImage alloc] init];
                [temp addRepresentation:pdfRep];
                NSBitmapImageRep *rep = [NSBitmapImageRep imageRepWithData:[temp TIFFRepresentation]];
                NSData *imageData = [rep representationUsingType:NSPNGFileType properties:@{}];
                
                NSString * resourceURLString =[NSString stringWithFormat:@"cid:%@",contentID];
                
                NSURL * resourceURL = [NSURL URLWithString:resourceURLString];
                WebResource * resource = [[WebResource alloc] initWithData: imageData
                                                                       URL: resourceURL
                                                                  MIMEType: @"image/png"
                                                          textEncodingName: @"base64"
                                                                 frameName: nil];
                self.internalWebResource  = resource;
                
                NSString * fileName = [self.contentDisposition[@"filename"] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\""]];
                CGFloat width = MIN(500,NSWidth(pdfRep.bounds));
                if (!width) width = 300;
                NSString * fileNamePart = [fileName length]?[NSString stringWithFormat:@"<br />%@",fileName]:@"";
                self.internalWebArchiveHtmlContent = [NSString stringWithFormat:@"<img src=\"%@\" width=\"%ld\" />%@",resourceURLString,(NSInteger)width,fileNamePart];
                
            }
            else{
                // need to display as generic icon.
            }
        }
    }
}

- (NSArray *) webArchiveResources {
    [self prepareWebResources];
    if (self.internalWebResource){
         return @[self.internalWebResource];
    }
    return nil;
}

-(NSString*) webArchiveHtmlContent{
    [self prepareWebResources];
    return self.internalWebArchiveHtmlContent;
}

@end
