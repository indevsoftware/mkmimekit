//
//  MKMimeHeadersParser.m
//  MKMimeKit
//
//  Created by Alex on 11-07-25.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "MKMimeHeadersParser.h"
#import "NSString+MimeEncoding.h"

@implementation MKMimeHeadersParser


+ (MKMimeHeaders *)headersFromString:(NSString *)aHeaderString {
    MKMimeHeadersParser * parser = [[MKMimeHeadersParser alloc] initWithHeaderString:aHeaderString];
    MKMimeHeaders * headers = [[MKMimeHeaders alloc] initWithHeaderDictionary:[parser headerDictionary]];
    
    return headers;
}
+ (MKMimeHeaders *)headersFromData:(NSData *)headerData {
    MKMimeHeadersParser * parser = [[MKMimeHeadersParser alloc] initWithHeaderData:headerData];
    MKMimeHeaders * headers = [[MKMimeHeaders alloc] initWithHeaderDictionary:[parser headerDictionary]];
    
    return headers;
}


- (id)initWithHeaderString:(NSString *)aHeaderString {
    self = [super init];
    
    if (self) {
        self.headerString = [aHeaderString copy];
    }
    
    return self;
}

-(id)initWithHeaderData:(NSData *)headerData{
    self = [super init];
    NSString * headerString = [[NSString alloc] initWithData:headerData encoding:NSASCIIStringEncoding];
    self.headerString = headerString;
    return self;
}

- (NSDictionary *)headerDictionary {
    NSScanner * headerScanner = [NSScanner scannerWithString:[self headerString]];
    [headerScanner setCharactersToBeSkipped:nil];
    
	NSMutableDictionary * headerDictionary = [NSMutableDictionary dictionary];
	NSString * headerKey = nil;
	NSMutableString * headerValue = nil;
    
	while (![headerScanner isAtEnd]) {
		NSString * headerLine = nil;
		[headerScanner scanUpToString:@"\n" intoString: &headerLine];
		[headerScanner scanString:@"\n" intoString:nil];
        
        if ([headerLine hasPrefix:@"\t"] || [headerLine hasPrefix:@" "]) {
			if (headerValue) {
				[headerValue appendFormat:@"\n%@", headerLine];
			}
		}
		else {
			if (headerKey && headerValue) {
                NSString * decodedValue = [headerValue decodedMimeEncodedString];
                if (!decodedValue) decodedValue = headerValue;

				if (![headerDictionary objectForKey:headerKey]) {
					[headerDictionary setObject:[NSMutableArray array] forKey:headerKey];
				}
				
                [[headerDictionary objectForKey:headerKey] addObject: [decodedValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			}
			
			NSScanner * headerKeyScanner = [NSScanner scannerWithString:headerLine];
			[headerKeyScanner scanUpToString:@":" intoString:&headerKey];
			[headerKeyScanner scanString:@":" intoString:nil];
			NSString *tempValue = nil;
			[headerKeyScanner scanUpToString:@"\n" intoString:&tempValue];
			headerValue = [tempValue mutableCopy];
		}
	}
    
	if (headerKey && headerValue) {
        
        NSString * decodedValue = [headerValue decodedMimeEncodedString];
        if (!decodedValue) decodedValue = headerValue;
        
		if (![headerDictionary objectForKey:headerKey]) {
			[headerDictionary setObject:[NSMutableArray array] forKey:headerKey];
		}
		
        [[headerDictionary objectForKey:headerKey] addObject: [decodedValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
	}
    
	return headerDictionary;
}

@end
