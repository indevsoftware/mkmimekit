//
//  MimePart.h
//  MKMimeKit
//
//  Created by Scott Morrison on 11-01-29.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import <Cocoa/Cocoa.h>

@class MKMimeHeaders,WebArchive;


extern NSString * const kMimeTransferEncodingBase64;// = @"base64";
extern NSString * const kMimeTransferEncoding8Bit;// = @"8bit";
extern NSString * const kMimeTransferEncoding7Bit;// = @"7bit";
extern NSString * const kMimeTransferEncodingQuotedPrintable;// = @"Quoted-Printable";


extern NSString * const kMimeHeaderContentTransferEncoding;// = @"Content-Transfer-Encoding";
extern NSString * const kMimeHeaderContentType;// = @"Content-Type";
extern NSString * const kMimeHeaderContentID;
extern NSString * const kMimeHeaderContentDisposition;

extern NSString * const kMimeContentEncodingISO8859;
extern NSString * const kMimeContentEncodingUTF8;
extern NSString * const kMimeContentEncodingUSASCII;



@interface MimePart : NSObject

@property (weak) MimePart * parent;
@property (strong) NSMutableArray * subParts;

@property (strong) MKMimeHeaders * headers;
@property (copy) NSData * contentData;
@property (copy) NSString * type;
@property (copy) NSString * subtype;
@property (copy) NSString * boundary;
@property (copy) NSString * contentTransferEncoding;
@property (copy) NSDictionary * contentDisposition;
@property (copy) NSString * contentId;
@property (copy) NSString * charset;
@property (readonly) NSString * MIMEtype;
@property (readonly) NSData * base64Data;
@property (readonly) NSString * filename;

- (id)initWithMimeData:(NSData *)mimeData;

- (void)loadAttachments;
- (NSArray <NSString*> *) attachments;
- (NSString *)htmlContent;
- (NSString *)plainContent;
- (NSData*) dataContent;
- (NSString*) webArchivePlainContent;
- (NSString*) webArchiveHtmlContent;

- (NSData*) webArchiveData;  // used only when MimePart is top levelPart
- (NSArray *) webArchiveResources;

- (NSStringEncoding)stringEncoding;
- (NSString*)htmlEnvelopeWithContents:(NSString*)contentString;

- (MimePart*)partWithContentID:(NSString*)contentID;
@end

@interface MimePart (ClassMethods)

+ (void)registerForMimeType:(NSString*)mimeType;
+ (Class)classForMimeType:(NSString*)mimeType;
+ (MimePart *)mimePartForData:(NSData *)mimeData;

@end

