//
//  ApplicationMimePart.m
//  MKMimeKit
//
//  Created by smorr on 2016-01-12.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "ApplicationMimePart.h"
#import <WebKit/WebKit.h>
#import "MKCommon.h"
#import "MKMimeHeaders.h"

@interface ApplicationMimePart ()
@property (strong) NSArray <WebResource*>* internalWebResources;
@property (strong) NSString* internalWebArchiveHtmlContent;
@property (strong) NSString* fileResourceURLString;
@property (strong) NSString* imageResourceURLString;
@end

@implementation ApplicationMimePart
+(void)load{
    [self registerForMimeType:@"application/*"];
}

+(NSString*)subtypeForFileAtPath:(NSString*)path{
    static NSDictionary * extensionTypeMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        extensionTypeMap = @{@"zip":@"zip",
                             @"pdf":@"pdf"};
    });
    NSString * extension = [path pathExtension];
    NSString * subtype = extensionTypeMap[extension];
    if (subtype){
        return subtype;
    }
    return @"unknown";
    
}

-(instancetype)initWithPathToFile:(NSString*)path{
    self = [super init];
    if (self){
        self.type = @"application";
        self.subtype = [ApplicationMimePart subtypeForFileAtPath:path];
        
        NSString * fileName = [path lastPathComponent];
        NSData * fileContents = [NSData dataWithContentsOfFile:path];
        NSData * base64Contents = [fileContents base64EncodedDataWithOptions:NSDataBase64Encoding76CharacterLineLength];
        
        self.headers = [[MKMimeHeaders alloc] init];
        [self.headers setHeader:@format(@"attachment; filename=\"%@\"",fileName) forKey:kMimeHeaderContentDisposition];
        [self.headers setHeader:@format(@"%@/%@; name=\"%@\"",self.type,self.subtype,fileName) forKey:kMimeHeaderContentType];
        [self.headers setHeader:@"base64" forKey:kMimeHeaderContentTransferEncoding];
        
        self.contentData = base64Contents;
         
    }
    return self;
}

-(instancetype)initWithMimeData:(NSData *)mimeData{
    self = [super initWithMimeData:mimeData];
    if (self){
        if (!self.contentId){
            self.contentId = [[NSUUID UUID] UUIDString];
        }
    }
    return self;
}


-(NSArray <NSString*> *) attachments{
    if ([self.contentDisposition[@"type"] isEqualToString:@"attachment"]){
        NSString * fileName = [self.contentDisposition[@"filename"] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\""]];
        if (fileName){
            return @[fileName];
        }
    }
    return nil;
}

- (NSData*) dataContent{
    NSString * transferEncoding =[[self contentTransferEncoding] lowercaseString];
    if ([transferEncoding isEqualToString:@"base64"]){
        NSData * fileData = [[NSData alloc] initWithBase64EncodedData: self.contentData options:    NSDataBase64DecodingIgnoreUnknownCharacters];
        return fileData;
    }
    else if ([transferEncoding isEqualToString:@"7bit"]){
         NSData * fileData = self.contentData;
        return fileData;
    }
    return nil;
}

-(void) prepareWebResources{
    if (!self.internalWebResources){
        NSString * transferEncoding =[[self contentTransferEncoding] lowercaseString];
        if ([transferEncoding isEqualToString:@"base64"]||
            [transferEncoding isEqualToString:@"7bit"]){
            NSString * imageContentID = [[NSUUID UUID] UUIDString];
            
            // to do, look up document image from extension.
            NSImage * documentIconImage = nil;
            
            NSString * fileName = [self.contentDisposition[@"filename"] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\""]];
            if ([fileName pathExtension]){
                documentIconImage = [[NSWorkspace sharedWorkspace] iconForFileType:[fileName pathExtension]];
            }
            if (!documentIconImage){
                documentIconImage =   [NSImage imageNamed:@"NSMysteryDocument"];
            }
            
            NSFont * font = [NSFont fontWithName:@"Helvetica Neue" size:12];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
            paragraphStyle.alignment = NSCenterTextAlignment;
            paragraphStyle.maximumLineHeight = 14.0;
            
            NSDictionary * attributes =@{NSFontAttributeName:font,
                                         NSParagraphStyleAttributeName:paragraphStyle};
            
            NSImage *iconWithNameImage = [[NSImage alloc] initWithSize:NSMakeSize(128, 128)] ;
            [iconWithNameImage lockFocus];
            {
                [[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
                [documentIconImage drawInRect:(NSRect){{32,56},{64,64}}];
                if (fileName){
                    NSRect textRect = NSMakeRect(5.5,5.5,118,40) ;
                    [[NSColor whiteColor] set];
                    NSRectFill(textRect);
                    [[NSGraphicsContext currentContext] setShouldAntialias:YES];
                    [fileName drawWithRect:textRect options:NSStringDrawingUsesLineFragmentOrigin attributes: attributes];
                }
            }
            [iconWithNameImage unlockFocus];
            
            NSData *imageData = [iconWithNameImage TIFFRepresentation];
            NSBitmapImageRep *imageRep = [NSBitmapImageRep imageRepWithData:imageData];
            
            
            imageData = [imageRep representationUsingType:NSPNGFileType properties:@{}];
            
            self.imageResourceURLString =[NSString stringWithFormat:@"cid:%@",imageContentID];
            
            NSURL * imageResourceURL = [NSURL URLWithString:self.imageResourceURLString];
            
            // webresources MUST be created on the main thread.
            
            WebResource * imageResource = [[WebResource alloc] initWithData: imageData
                                                                        URL: imageResourceURL
                                                                   MIMEType: @"image/png"
                                                           textEncodingName: @""
                                                                  frameName: nil];
            
            
            NSString * fileContentID = [self.contentId stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
            self.fileResourceURLString =[NSString stringWithFormat:@"cid:%@",fileContentID];
            NSURL * fileResourceURL = [NSURL URLWithString:self.fileResourceURLString];
            
            NSData * fileData = self.dataContent;
            
            WebResource * fileResource = [[WebResource alloc] initWithData: fileData
                                                                       URL: fileResourceURL
                                                                  MIMEType: self.MIMEtype
                                                          textEncodingName: self.contentTransferEncoding
                                                                 frameName: nil];
            
            self.internalWebResources = @[imageResource,fileResource];
            
            NSString * htmlString = [NSString stringWithFormat:@"<a href=\"%@\"><img src=\"%@\" width=\"128\" style=\"border: 2px dashed #ccc; -webkit-border-radius:15px\"/></a>",self.fileResourceURLString,self.imageResourceURLString];
            self.internalWebArchiveHtmlContent = htmlString;

        }
    }
}

- (NSArray *) webArchiveResources {
    [self prepareWebResources];
    return self.internalWebResources;
}

-(NSString*) webArchiveHtmlContent{
    [self prepareWebResources];
    return self.internalWebArchiveHtmlContent;
}

-(NSArray *)writableTypesForPasteboard:(NSPasteboard *)pasteboard{
    return @[self.subtype];
}
- (NSPasteboardWritingOptions)writingOptionsForType:(NSString *)type pasteboard:(NSPasteboard *)pasteboard{
    return NSPasteboardWritingPromised;
}

- (id)pasteboardPropertyListForType:(NSString *)type{
    return self.dataContent;
}
@end
