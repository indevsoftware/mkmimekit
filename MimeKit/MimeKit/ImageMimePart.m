//
//  ImageMimePart.m
//  MKMimeKit
//
//  Created by smorr on 2016-01-12.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "ImageMimePart.h"
#import <WebKit/WebKit.h>
#import "MKMimeHeaders.h"
#import "MKCommon.h"


NSString * const kMimeImageSubtypeJpg = @"jpeg";
NSString * const kMimeImageSubtypePng = @"png";

@implementation ImageMimePart
+(void)load{
    [self registerForMimeType:@"image/*"];
}

-(instancetype)initWithData:(NSData*) data inlineId:(NSString*)inlineId subtype:(NSString*)mimeSubType{
    self = [super init];
    if (self){
        self.type = @"image";
        self.subtype = mimeSubType;
        
        NSString * fileName = [[inlineId componentsSeparatedByString:@"@"] firstObject];
        NSData * base64Contents = [data base64EncodedDataWithOptions:NSDataBase64Encoding76CharacterLineLength];
        
        self.headers = [[MKMimeHeaders alloc] init];
        if (inlineId){
            [self.headers setHeader:@format(@"inline;\r\n filename=%@",fileName) forKey:kMimeHeaderContentDisposition];
            [self.headers setHeader:@format(@"<%@>",inlineId) forKey:kMimeHeaderContentID];
        }
        else{
            [self.headers setHeader:@format(@"attachment;\r\n filename=\"%@\"",fileName) forKey:kMimeHeaderContentDisposition];
        }
        
        [self.headers setHeader:@format(@"%@/%@;\r\n x-unix-mode=0600;\r\n name=\"%@\"",self.type,self.subtype,fileName) forKey:kMimeHeaderContentType];
        [self.headers setHeader:@"base64" forKey:kMimeHeaderContentTransferEncoding];
        
        self.contentData = base64Contents;
        
    }
    return self;
}

-(instancetype)initWithPathToFile:(NSString*)path inlineId:(NSString*)inlineId subtype:(NSString*)mimeSubType{
    self = [super init];
    if (self){
        self.type = @"image";
        self.subtype = mimeSubType;
        
        NSString * fileName = [path lastPathComponent];
        NSData * fileContents = [NSData dataWithContentsOfFile:path];
        NSData * base64Contents = [fileContents base64EncodedDataWithOptions:NSDataBase64Encoding76CharacterLineLength];
        
        self.headers = [[MKMimeHeaders alloc] init];
        if (inlineId){
            [self.headers setHeader:@format(@"inline;\r\n filename=%@",fileName) forKey:kMimeHeaderContentDisposition];
            [self.headers setHeader:@format(@"<%@>",inlineId) forKey:kMimeHeaderContentID];
        }
        else{
            [self.headers setHeader:@format(@"attachment;\r\n filename=\"%@\"",fileName) forKey:kMimeHeaderContentDisposition];
        }
        
        [self.headers setHeader:@format(@"%@/%@;\r\n x-unix-mode=0600;\r\n name=\"%@\"",self.type,self.subtype,fileName) forKey:kMimeHeaderContentType];
        [self.headers setHeader:@"base64" forKey:kMimeHeaderContentTransferEncoding];
        
        self.contentData = base64Contents;
        
    }
    return self;
}

-(instancetype)initWithMimeData:(NSData *)mimeData{
    self = [super initWithMimeData:mimeData];
    if (self){
        if (!self.contentId){
            self.contentId = [[NSUUID UUID] UUIDString];
        }
    }
    return self;
}
    
- (NSData*) dataContent{
    return nil;
}

- (NSArray *) webArchiveResources {
    if ([[[self contentTransferEncoding] lowercaseString] isEqualToString:@"base64"]){
        NSString * contentID = [self.contentId stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
        
        NSString * resourceURLString =[NSString stringWithFormat:@"cid:%@",contentID];
        
        NSURL * resourceURL = [NSURL URLWithString:resourceURLString];
        NSData * rawData = [[NSData alloc] initWithBase64EncodedData: self.contentData options:    NSDataBase64DecodingIgnoreUnknownCharacters];
        WebResource * resource = [[WebResource alloc] initWithData: rawData
                                                               URL: resourceURL
                                                          MIMEType: [self MIMEtype]
                                                  textEncodingName: @"base64"
                                                         frameName: nil];
        if (resource){
            return @[resource];
        }
    }
    return nil;
}

-(NSString*) webArchiveHtmlContent{
    if ([[[self contentTransferEncoding] lowercaseString] isEqualToString:@"base64"]){
        NSString * contentID = [self.contentId stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
        
        NSString * resourceURLString =[NSString stringWithFormat:@"cid:%@",contentID];
        
        NSString * imageFileName = [[self contentDisposition] objectForKey:@"filename"];
        NSString * imageAlt = imageFileName?[NSString stringWithFormat:@"alt=\"%@\"",imageFileName]:@"";
        
        NSString * imgHtml = [NSString stringWithFormat:@"<p><img src=\"%@\" %@/><br />%@</p>",resourceURLString,imageAlt,imageFileName];
        return imgHtml;
    }
    return nil;

}

-(NSData*) webArchiveData{
    NSString * html = [self webArchiveHtmlContent];
    if (html){
        return [[self htmlEnvelopeWithContents:html] dataUsingEncoding:NSUTF8StringEncoding];
    }
    return nil;
}
@end
