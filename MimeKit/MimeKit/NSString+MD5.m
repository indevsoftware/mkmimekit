//
//  NSString+MD5.m
//  MKMimeKit
//
//  Created by Alex Krolik on 12-07-20.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "NSString+MD5.h"

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import "NSData+Base64.h"

@implementation NSString (NSString_MD5)

- (NSString *)hexMD5String {
    const char *cStr = [self UTF8String];
    
    unsigned char md5[CC_MD5_DIGEST_LENGTH];
    bzero(md5, CC_MD5_DIGEST_LENGTH);
    
    CC_MD5(cStr, strlen(cStr), md5);
    
    NSData * md5Data = [[NSData alloc] initWithBytes:md5 length:sizeof(md5)];
    
    NSMutableString * md5HexString = [[NSMutableString alloc] init];
	const char * md5Bytes = [md5Data bytes];
    
	for (int i = 0; i < [md5Data length]; ++i) {
        const unsigned char byte = md5Bytes[i];
        
		[md5HexString appendFormat:@"%02x", byte];
	}
    
    return md5HexString;
}

- (NSString *)hexMD5StringWithKey:(NSString *)key {
    const char * ckey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char * data = [self cStringUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char md5[CC_MD5_DIGEST_LENGTH];
    bzero(md5, CC_MD5_DIGEST_LENGTH);
    
    CCHmac(kCCHmacAlgMD5, ckey, strlen(ckey), data, strlen(data), md5);
    
    NSData * hmacData = [[NSData alloc] initWithBytes:md5 length:sizeof(md5)];
    
    NSMutableString * hmacHexString = [[NSMutableString alloc] init];
	const char * hmacBytes = [hmacData bytes];
    
	for (int i = 0; i < [hmacData length]; ++i) {
        const unsigned char byte = hmacBytes[i];
        
		[hmacHexString appendFormat:@"%02x", byte];
	}
    
    return hmacHexString;
}

-(NSString*)shortSHAHashString{
#define ID_SIZE 12
    const char *s=[self cStringUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(s, (CC_LONG)strlen(s), digest);
    
    NSData * digestData = [NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    
    NSString * generatedID =[[digestData pathSafeBase64EncodedString] substringToIndex:ID_SIZE];
    return generatedID;
    
}

- (NSString *)sha256HashString {
	const char *s = [self cStringUsingEncoding:NSUTF8StringEncoding];
	
	uint8_t digest[CC_SHA256_DIGEST_LENGTH] = {0};
	CC_SHA256(s, (CC_LONG)strlen(s), digest);
	
	NSMutableString * sha256String = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
	for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
		[sha256String appendFormat:@"%02x", digest[i]];
	}
	
	return sha256String;
}

@end
