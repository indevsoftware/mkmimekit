//
//  NSString+MimeEncoding.h
//  EmailAddressParser
//
//  Created by smorr on 2015-01-15.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import <Foundation/Foundation.h>

@interface NSString (MimeEncoding)


/* Method +mimeWordWithString:preferredEncoding:encodingUsed:
 
    Returns a full mimeword encoded string for the provided string using the preferred encoding if possible
        If the provided string cannot be encoded with the preferred, it will use alternate encodings and return encoding used
        in the reference parameter.
 
    eg [NSString mimeWordWithString:@"Garçon" preferredEncoding:NSISOLatin1StringEncoding encodingUsed:0]
            returns @"=?iso-8859-1?Q?Gar=E7on?="
 
       [NSString mimeWordWithString:@"Garçon" preferredEncoding:NSUTF8StringEncoding encodingUsed:0]
            returns @"=?utf-8?Q?Gar=C3=A7on?="

 */

+ (NSString*) mimeWordWithString:(NSString*) string preferredEncoding:(NSStringEncoding)encoding encodingUsed:(NSStringEncoding*)usedEncoding;



/* Method +qEncodedStringWithString:preferredEncoding:encodingUsed:
    returns the quotedPrintableStringWithout the Mime Type Envelope.  See ( https://tools.ietf.org/html/rfc2047#section-4.2 )
 
   eg [NSString quotedPrintableStringWithString:@"Garçon" preferredEncoding:NSISOLatin1StringEncoding encodingUsed:0] 
        returns @"Gar=E7on"
 
    This should not be used for actual mime words but is provided so that methods can break the quotedPrintable at set length
    for headers or other purposes.
 
    NOTE -- in contravention of the RFC for QuotedPrintableStrings, the return value is not wrapped at 75 characters.  
            Wrapping is to be performed by client method.
*/


+ (NSString*) qEncodedStringWithString:(NSString *)string preferredEncoding:(NSStringEncoding)preferredEncoding encodingUsed:(NSStringEncoding *)usedEncoding;


+ (NSString*)quotedPrintableStringForPlainTextBody:(NSString *)string preferredEncoding:(NSStringEncoding)preferredEncoding encodingUsed:(NSStringEncoding *)usedEncoding;

+ (NSString*) MKcharSetNameForEncoding:(NSStringEncoding)encoding;
+ (NSStringEncoding) MKencodingForCharSetName:(NSString*)charSet;

+ (NSString*) stringWithMimeEncodedWord:(NSString*)word;
- (NSString*) decodedMimeEncodedString;

@end

