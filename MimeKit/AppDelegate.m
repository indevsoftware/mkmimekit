//
//  AppDelegate.m
//  MimeKit
//
//  Created by smorr on 2016-01-14.
//
//  Copyright (c) 2016 SmallCubed Software
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
//  to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import "AppDelegate.h"
#import "MimePart.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@property (strong) MimePart * topMimePart;

@end

@implementation AppDelegate

-(WebArchive*) webArchive{
    NSArray * webResources = [self.topMimePart webArchiveResources];
    NSData * archiveData= [self.topMimePart webArchiveData];
    WebResource * mainResource = [[WebResource alloc] initWithData:archiveData URL:[NSURL URLWithString:@""] MIMEType:@"text/html" textEncodingName:nil frameName:nil];
    
    WebArchive * archive= [[WebArchive alloc] initWithMainResource:mainResource subresources:webResources subframeArchives:nil];
    
    return archive;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
     NSString * path = [[NSBundle  bundleForClass:[self class]] pathForResource:@"6630E5aDu447FL-IEV" ofType:@"meml"];
    
    NSData * mimeData = [NSData dataWithContentsOfFile:path];
    MimePart * topPart= [MimePart mimePartForData:mimeData];
    self.topMimePart = topPart;
    
    WebArchive * webArchive = [self webArchive];
    if (webArchive){
        WebPreferences * viewPrefs = [self.webView preferences];
        [viewPrefs setStandardFontFamily:@"Helvetica Neue"];
        [viewPrefs setDefaultFontSize:12];
        
        [viewPrefs setJavaScriptEnabled:NO];
        [viewPrefs setLoadsImagesAutomatically:YES];
        
        [[self.webView mainFrame] loadArchive:webArchive];
    }

}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
